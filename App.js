import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './src/Screen/LoginScreen/index';
import SplashScreen from './src/Screen/SplashScreen/index';
import SignUpScreen from './src/Screen/SignUpScreen/index';
import Slider from './src/Screen/Slider/index';
import TabScreen from "./src/navigation/TabScreen";
const Stack = createStackNavigator();

function App() {


  return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen name="Splash" component={SplashScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name="SignUp" component={SignUpScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Slide" component={Slider} options={{ headerShown: false }} />
          <Stack.Screen name="Tab" component={TabScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
  );
}


export default App;

