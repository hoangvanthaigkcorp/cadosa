import Api from '../Api/APIConstant';

export const addCart = (id) => {
    return fetch(Api.baseUrl+Api.cart, {
        method: 'POST',
        headers: {
            Authorization:'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZDFlNDVlZjNhN2Y1N2U0OTczNWZjOCIsImlhdCI6MTYwODg2MjUwOCwiZXhwIjoxNjQwMzk4NTA4fQ.kv7G7Sc42MlQnRtJ5Mn6N1lx02ZT8EDE3-u1H2ViVYo',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            productId: id,
            quantity: "1"
        })
    })
}