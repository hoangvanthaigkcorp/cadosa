import axios from 'axios'
import Api from '../Api/APIConstant'

export const fetchProducts = () => {
    let url = '?pageIndex=1&pageSize=6'
    return fetch(Api.baseUrl + Api.product + url, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
}
export const fetchFlashSaleProducts = () => {
    let url = '?pageSize=6&pageIndex=1'
    return fetch(Api.baseUrl + Api.flashSale + url, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
}
export const fetchPost = () => {
    return fetch(Api.baseUrl + Api.post, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
}
export const fetchCart = () => {
    return fetch(Api.baseUrl + Api.cart, {
        method: 'GET',
        headers: {
            Authorization: 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZDFlNDVlZjNhN2Y1N2U0OTczNWZjOCIsImlhdCI6MTYwODg2MjUwOCwiZXhwIjoxNjQwMzk4NTA4fQ.kv7G7Sc42MlQnRtJ5Mn6N1lx02ZT8EDE3-u1H2ViVYo',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
}
export const fetchCategory = () => {
    return fetch(Api.baseUrl + Api.category, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })
}