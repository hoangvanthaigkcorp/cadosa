import Api from '../Api/APIConstant'

export const login = (Email, Password) => {
    return fetch(Api.baseUrl+Api.login, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: Email,
            password: Password,
        })
    })
}
