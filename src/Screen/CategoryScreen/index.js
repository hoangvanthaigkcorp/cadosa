import React, { useEffect, useState } from 'react';

import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Dimensions, Platform, NativeModules } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { fetchCategory } from '../../service/fetch';
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : 10;
const windowWidth = Dimensions.get('screen').width;
const windowHeight = Dimensions.get('screen').height;

export default function CategoryScreen({ navigation }) {

    const [categories, setCategories] = useState([])

    useEffect(() => {
        fetchCategory()
            .then((response) => response.json())
            .then((json) => {
                setCategories(json.data);
                // console.log(json.data);
            })
            .catch((error) => {
                console.error(error);
            });
    }, [])


    const renderCategory = () => {
        return categories.map((category) => {
            return (
                <View style={{ margin: 10 }} key={category._id}>
                    <TouchableOpacity >
                        <View style={{ backgroundColor: '#fff', width: windowWidth , height: windowHeight * 0.15, borderRadius: 30, justifyContent: 'center' }}>
                            <Text style={{ color: '#52BE59', textAlign: 'center' }}>{category.description}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        })
    }

    return (
        <View style={{ flex: 1, alignItems: 'flex-start', marginTop: 10, position: 'absolute' }}>
            <View style={{ flexDirection: 'row', width: windowWidth, height: 45, backgroundColor: '#52BE59', marginTop: -STATUSBAR_HEIGHT }}>
                <TouchableOpacity onPress={() => { navigation.goBack() }}>
                    <Image style={{ marginLeft: 10, marginTop: STATUSBAR_HEIGHT, marginRight: 0.35 * windowWidth }} source={require('../../Image/back.png')} />
                </TouchableOpacity>
                <Text style={{ flexDirection: 'row', textAlign: 'center', fontSize: 18, color: '#fff', marginTop: STATUSBAR_HEIGHT, textAlign: "center" }}>Danh mục</Text>
            </View>
            {renderCategory()}
        </View >

    );
}
