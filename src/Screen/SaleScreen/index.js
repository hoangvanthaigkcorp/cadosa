import { View, Text } from 'react-native';
import React from 'react';

export default function SaleScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Sale!</Text>
        </View>
    );
}