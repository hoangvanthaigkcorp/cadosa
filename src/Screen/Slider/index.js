// import React in our code
import React, { useState } from 'react';

// import all the components we are going to use
import {
    SafeAreaView,
    StyleSheet,
    View,
    Text,
    Image,
    Button,
} from 'react-native';

//import AppIntroSlider to use it
import AppIntroSlider from 'react-native-app-intro-slider';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Slider = ({ navigation }) => {

    const onDone = () => {
        navigation.navigate('Login');
    };
    const onSkip = () => {
        navigation.navigate('Login');
    };

    const RenderItem = ({ item }) => {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingBottom: 100,
                    marginTop:100
                }}>
                <Image
                    source={item.image} />
            </View>
        );
    };

    const renderNextButton = () => {
        return (
            <TouchableOpacity>
                <View>
                    <Image source={require('../../Image/next.png')} />
                </View>
            </TouchableOpacity>
        );
    }
    const renderSkipButton = () => {
        return (
            <TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 25, color: '#52BE59' }}>Bỏ qua</Text>
                </View>
            </TouchableOpacity>
        );
    }

    const renderDoneButton = () => {
        return (
            <TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 25, color: '#52BE59' }}>Xong</Text>
                </View>
            </TouchableOpacity>
        );
    }

    return (
        <AppIntroSlider
            data={slides}
            renderItem={RenderItem}
            onDone={onDone}
            showSkipButton={true}
            onSkip={onSkip}
            renderNextButton={renderNextButton}
            renderSkipButton={renderSkipButton}
            renderDoneButton={renderDoneButton}
            activeDotStyle={{ backgroundColor: '#52BE59' }}
        />
    );
};

export default Slider;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        padding: 10,
        justifyContent: 'center',
    },
});

const slides = [
    {
        key: 's1',
        image: require('../../Image/slide1.png'),
    },
    {
        key: 's2',
        image: require('../../Image/slide2.png'),
    },
    {
        key: 's3',
        image: require('../../Image/slide3.png'),
    },
    {
        key: 's4',
        image: require('../../Image/slide4.png'),
    }
];

