import { View, ImageBackground, Image, Dimensions, Text, Alert } from 'react-native';
import React, { useState } from 'react';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { login } from '../../service/auth';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function LoginScreen({ navigation }) {
    const [email, setEmail] = useState('phamtiennam2301@gmail.com');
    const [password, setPass] = useState('Phamtiennam2301');


    const pressLogin = () => {
        if (email.length == 0 || password.length == 0) {
            Alert.alert('Vui lòng điền đầy đủ email và mật khẩu')
        }
        else {
            login(email, password)
                .then((response) => response.json())
                .then((json) => {
                    if (json.message == "Thành công") {
                        navigation.navigate('Tab')
                    }
                })
                .catch((error) => {
                    console.error(error);
                });

        }
    }

    return (
        <ImageBackground source={require('../../Image/background.png')} style={{ width: windowWidth, height: windowHeight }}>
            <View style={{ flex: 1, alignItems: 'center' }}>
                <Image source={require('../../Image/logo.png')} style={{ marginTop: windowHeight * 0.1 }} />
                <View style={{ flexDirection: 'row', marginTop: windowHeight * 0.04 }}>
                    <Text style={{ fontSize: 18, color: '#52BE59', backgroundColor: 'white', borderRadius: 10, width: windowWidth * 0.3, textAlign: 'center', padding: 5, marginRight: windowWidth * 0.15, overflow: 'hidden' }}>Đăng nhập</Text>
                    <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                        <Text style={{ fontSize: 18, color: 'white', borderColor: 'white', borderWidth: 2, borderRadius: 10, width: windowWidth * 0.3, textAlign: 'center', padding: 5 }}>Đăng ký</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ width: windowWidth * 0.75, marginTop: 40 }}>
                    <Text style={{ color: 'white', marginBottom: -15, fontSize: 17 }}>Tên đăng nhập:</Text>
                    <TextInput style={{ borderBottomColor: 'white', borderBottomWidth: 1, }} onChangeText={(text) => setEmail(text)} value={email} />
                </View>
                <View style={{ width: windowWidth * 0.75, marginTop: 40 }}>
                    <Text style={{ color: 'white', marginBottom: -15, fontSize: 17 }}>Mật khẩu:</Text>
                    <TextInput secureTextEntry={true} style={{ borderBottomColor: 'white', borderBottomWidth: 1, }} onChangeText={(text) => setPass(text)} value={password} />
                </View>
                <TouchableOpacity onPress={() => {
                    // navigation.navigate('Slide')
                    pressLogin();
                }}>
                    <Text style={{ fontSize: 20, color: 'white', borderColor: 'white', borderWidth: 2, borderRadius: 10, width: windowWidth * 0.3, textAlign: 'center', padding: 5, marginTop: 40, marginLeft: windowWidth * 0.5 }}>Xác nhận</Text>
                </TouchableOpacity>
            </View>
        </ImageBackground>
    );
}