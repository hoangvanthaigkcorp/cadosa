import { View, Text, Dimensions, Image, ImageBackground, Alert } from 'react-native';
import Header from '../../components/Header';
import React, { useEffect, useState } from 'react';
import { fetchProducts, fetchFlashSaleProducts, fetchPost, fetchCart } from '../../service/fetch';
import { ScrollView, TextInput, TouchableHighlight, TouchableOpacity, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { addCart } from '../../service/add'
import moment from 'moment';

const url = 'http://188.166.251.69:6868'
const windowWidth = Dimensions.get('screen').width;
const windowHeight = Dimensions.get('screen').height;

export default function HomeScreen({ navigation }) {
    const [products, setProducts] = useState([]);
    const [flashSale, setFlashSale] = useState([]);
    const [posts, setPosts] = useState([]);

    const [cart, setCart] = useState({ total: '0', totalPrice: '0' });
    const [item, setItem] = useState({});


    useEffect(() => {
        fetchProducts()
            .then((response) => response.json())
            .then((json) => {
                setProducts(json.data.items);
            })
            .catch((error) => console.error(error))
    }, [])

    useEffect(() => {
        fetchFlashSaleProducts()
            .then((response) => response.json())
            .then((json) => {
                setFlashSale(json.data[0].productsFlashSales);
            })
            .catch((error) => console.error(error))
    }, [])

    useEffect(() => {
        fetchPost()
            .then((response) => response.json())
            .then((json) => {
                setPosts(json.data.items);
            })
            .catch((error) => console.error(error))
    }, [])

    useEffect(() => {
        fetchCart()
            .then((response) => response.json())
            .then((json) => {
                setCart({ total: json.data.total, totalPrice: json.data.totalPrice });
            })
            .catch((error) => console.error(error))
    }, [item])

    const addToCart = (id) => {
        // console.log({evt})
        // if (evt) {
        //     evt.stopPropagation();
        // }
        addCart(id)
            .then((response) => response.json())
            .then((json) => {
                Alert.alert(json.message)
                if (json.message == "Thành công") {
                    setItem(json.data)
                }
            })
            .catch((error) => console.error(error))
    }

    const renderProducts = () => {
        return products.map((product) => {
            return (
                <View key={product._id} style={{ marginLeft: 20, marginTop: 20, position: 'relative' }}>
                    <View style={{ backgroundColor: '#fff', width: windowWidth * 0.42, height: windowHeight * 0.15, borderRadius: 30 }}>
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('Detail');
                        }} >
                            <Image source={{ uri: url + product.thumbnail }} style={{ width: windowWidth * 0.42, height: windowHeight * 0.15, borderRadius: 30 }} />
                        </TouchableOpacity >
                        <View style={{ position: 'absolute', top: 0, right: -5 }}>
                            <TouchableOpacity onPress={() => {
                                addToCart(product._id);
                            }}
                            >
                                <Image source={require('../../Image/addtocart.png')} style={{ marginLeft: 0 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text style={{ color: '#52BE59', textAlign: 'center' }}>{product.name}</Text>
                    <Text style={{ color: '#8C8C8C', textAlign: 'center' }}>{product.price} đ</Text>
                </View>
            )
        })
    }


    const renderFlashSale = () => {
        return flashSale.map((product) => {
            return (
                <View key={product._id} style={{ marginLeft: 20, marginTop: 20, position: 'relative' }}>
                    <View style={{ backgroundColor: '#fff', width: windowWidth * 0.42, height: windowHeight * 0.15, borderRadius: 30 }}>
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('Detail');
                        }} >
                            <Image source={{ uri: url + product.thumbnail }} style={{ width: windowWidth * 0.42, height: windowHeight * 0.15, borderRadius: 30 }} />
                        </TouchableOpacity >
                        <View style={{ position: 'absolute', top: 0, right: -5 }}>
                            <TouchableOpacity onPress={() => {
                                addToCart(product._id);
                            }}
                            >
                                <Image source={require('../../Image/addtocart.png')} style={{ marginLeft: 0 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text style={{ color: '#52BE59', textAlign: 'center' }}>{product.name}</Text>
                    <Text style={{ color: '#8C8C8C', textAlign: 'center' }}>{product.price} đ</Text>
                </View>
            )
        })
    }

    const renderPosts = () => {
        return posts.map((post) => {
            return (
                <View key={post._id} style={{ marginLeft: 20, marginTop: 20 }}>
                    <View style={{ backgroundColor: '#fff', width: windowWidth * 0.42, height: windowHeight * 0.15, borderRadius: 30 }}>
                        <Image source={{ uri: url + post.thumbnailUrl }} style={{ width: windowWidth * 0.42, height: windowHeight * 0.15, resizeMode: 'contain', borderRadius: 30 }} />
                    </View>
                    <Text style={{ color: '#000000', textAlign: 'center', fontStyle: 'italic', marginTop: 10 }}>{moment(post.createdAt).format("DD/MM/YYYY")}</Text>
                    <Text style={{ color: '#52BE59', textAlign: 'center', fontSize: 15 }}>{post.title}</Text>
                </View>
            )
        })
    }

    return (
        <View style={{ flex: 1, alignItems: 'flex-start', marginTop: 10 }}>
            <Header title='Trang chủ' navigation={navigation}/>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 45, marginTop: 10, marginLeft: 10, marginBottom: 20 }}>
                    <View style={{ flexDirection: 'row', backgroundColor: '#CCFFC4', borderRadius: 50, width: windowWidth * 0.8, height: 43 }}>
                        <Image source={require('../../Image/search.png')} style={{ width: 45, height: 45 }} />
                        <TextInput placeholder="Tìm kiếm" />
                    </View>
                    <TouchableOpacity onPress={() => { Alert.alert(cart.totalPrice.toString()) }}>
                        <ImageBackground source={require('../../Image/cart.png')} style={{ width: 60, height: 60 }}>
                            <ImageBackground source={require('../../Image/cartnoti.png')} style={{ marginTop: 10, marginLeft: 40, width: 15, height: 15 }}>
                                <Text style={{ color: 'white', marginLeft: 5, fontSize: 11 }}>{cart.total}</Text>
                            </ImageBackground>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
                <View style={{ borderBottomColor: '#52BE59', borderBottomWidth: 3, width: windowWidth * 0.43, marginLeft: 15 }}>
                    <Text style={{ color: '#52BE59', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Giá sốc hôm nay:</Text>
                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {renderFlashSale()}
                </View>
                <View style={{ borderBottomColor: '#52BE59', borderBottomWidth: 3, width: windowWidth * 0.43, marginLeft: 15 }}>
                    <Text style={{ color: '#52BE59', fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Sản phẩm nổi bật:</Text>
                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {renderProducts()}
                </View>
                <Text style={{ color: '#52BE59', fontSize: 20, fontWeight: 'bold', textAlign: 'center', width: windowWidth }}>Câu chuyện của Cadosa</Text>
                <View style={{ width: windowWidth, alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={require('../../Image/story_logo.png')} />
                </View>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginBottom: 20 }}>
                    {renderPosts()}
                </View>
            </ScrollView>
        </View >

    );
}
