import { View, Image, ImageBackground, Dimensions, Text } from 'react-native';
import * as React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';


const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const B = (props) => <Text style={{ fontWeight: 'bold', color: '#FF9A9A' }}>{props.children}</Text>
export default function SplashScreen({ navigation }) {
    return (
        <ImageBackground source={require('../../Image/background.png')} style={{ width: windowWidth, height: windowHeight }}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={require('../../Image/logo.png')} />
                <TouchableOpacity onPress={() => {
                    navigation.navigate('Login')
                }}>
                    <Text style={{ fontSize: 29, fontWeight: 'bold', color: 'white', borderColor: 'white', borderWidth: 3, borderRadius: 10, width: windowWidth * 0.65, textAlign: 'center', padding: 5, marginTop: windowHeight * 0.09 }}>Bắt Đầu</Text>
                </TouchableOpacity>
                <Text style={{ width: windowWidth * 0.8, textAlign: 'center', fontSize: 14, color: '#fff', bottom: -windowHeight * 0.2 }}>By joining your agree to ours <B>Terms of Service</B> and <B>Privacy Policy</B></Text>
            </View>
        </ImageBackground>
    );
}