import { View, ImageBackground, Image, Dimensions, Text } from 'react-native';
import * as React from 'react';
import { ScrollView, TextInput, TouchableOpacity } from 'react-native-gesture-handler';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function SignUpScreen({ navigation }) {
    return (
        <ImageBackground source={require('../../Image/background.png')} style={{ width: windowWidth, height: windowHeight }}>
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center',alignItems:'center' }} showsVerticalScrollIndicator={false}>
                <Image source={require('../../Image/logo.png')} style={{ marginTop: windowHeight * 0.1}} />
                <View style={{ flexDirection: 'row', marginTop: windowHeight * 0.04 }}>
                    <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                        <Text style={{ fontSize: 18, color: 'white', borderColor: 'white', borderWidth: 2, borderRadius: 10, width: windowWidth * 0.3, textAlign: 'center', padding: 5, marginRight: windowWidth * 0.15 }}>Đăng nhập</Text>
                    </TouchableOpacity>
                    <Text style={{ fontSize: 18, color: '#52BE59', backgroundColor: 'white', borderRadius: 10, width: windowWidth * 0.3, textAlign: 'center', padding: 5 ,overflow:'hidden'}}>Đăng ký</Text>
                </View>
                <View style={{ width: windowWidth * 0.75, marginTop: 40 }}>
                    <Text style={{ color: 'white', marginBottom: -15, fontSize: 17 }}>Tên đăng nhập:</Text>
                    <TextInput style={{ borderBottomColor: 'white', borderBottomWidth: 1, }} />
                </View>
                <View style={{ width: windowWidth * 0.75, marginTop: 40 }}>
                    <Text style={{ color: 'white', marginBottom: -15, fontSize: 17 }}>Mật khẩu:</Text>
                    <TextInput  secureTextEntry={true} style={{ borderBottomColor: 'white', borderBottomWidth: 1, }} />
                </View>
                <View style={{ width: windowWidth * 0.75, marginTop: 40 }}>
                    <Text style={{ color: 'white', marginBottom: -15, fontSize: 17 }}>Nhập lại mật khẩu:</Text>
                    <TextInput secureTextEntry={true} style={{ borderBottomColor: 'white', borderBottomWidth: 1, }} />
                </View>
                <View style={{ width: windowWidth * 0.75, marginTop: 40 }}>
                    <Text style={{ color: 'white', marginBottom: -15, fontSize: 17 }}>Email:</Text>
                    <TextInput style={{ borderBottomColor: 'white', borderBottomWidth: 1, }} />
                </View>
                <TouchableOpacity onPress={() => {

                }}>
                    <Text style={{ fontSize: 20, color: 'white', borderColor: 'white', borderWidth: 2, borderRadius: 10, width: windowWidth * 0.3, textAlign: 'center', padding: 5, marginTop: 40,marginBottom:60,marginLeft:windowWidth*0.5 }}>Xác nhận</Text>
                </TouchableOpacity>
            </ScrollView>
        </ImageBackground>
    );
}