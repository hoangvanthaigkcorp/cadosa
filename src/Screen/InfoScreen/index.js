import { View, Text } from 'react-native';
import React from 'react';

export default function InfoScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Info!</Text>
        </View>
    );
}