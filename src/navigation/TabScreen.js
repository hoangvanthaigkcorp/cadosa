import { createStackNavigator } from '@react-navigation/stack';
import InfoScreen from '../Screen/InfoScreen/index';
import HomeScreen from '../Screen/HomeScreen/index';
import WalletScreen from '../Screen/WalletScreen/index';
import SaleScreen from '../Screen/SaleScreen/index';
import NotificationScreen from '../Screen/NotiScreen/index';
import DetailScreen from '../Screen/DetailScreen/index';
import CategoryScreen from '../Screen/CategoryScreen/index';
import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Image, View, TouchableWithoutFeedback, Text, Platform } from 'react-native';


const bottomHeight = Platform.OS === 'ios' ? 0 : -10;
const height = Platform.OS === 'ios' ? 80 : 70;
//bottom navigation
const HomeStack = createStackNavigator();
const HomeStackScreen = () => {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen name="Home" component={HomeScreen} options={{
                headerShown: false,
            }} />
            <HomeStack.Screen name="Detail" component={DetailScreen} options={{
                headerShown: false,
            }} />
            <HomeStack.Screen name="Category" component={CategoryScreen} options={{
                headerShown: false,
            }} />
        </HomeStack.Navigator>
    );
}
const WalletStack = createStackNavigator();
const WalletStackScreen = () => {
    return (
        <WalletStack.Navigator>
            <WalletStack.Screen name="Wallet" component={WalletScreen} options={{
                headerShown: false,
            }} />
        </WalletStack.Navigator>
    );
}
const SaleStack = createStackNavigator();
const SaleStackScreen = () => {
    return (
        <SaleStack.Navigator>
            <SaleStack.Screen name="Sale" component={SaleScreen} options={{
                headerShown: false,
            }} />
        </SaleStack.Navigator>
    );
}
const NotificationsStack = createStackNavigator();
const NotificationsStackScreen = () => {
    return (
        <NotificationsStack.Navigator>
            <NotificationsStack.Screen name="Noti" component={NotificationScreen} options={{
                headerShown: false,
            }} />
        </NotificationsStack.Navigator>
    );
}
const InfoStack = createStackNavigator();
const InfoStackScreen = () => {
    return (
        <InfoStack.Navigator>
            <InfoStack.Screen name="Info" component={InfoScreen} options={{
                headerShown: false,
            }} />
        </InfoStack.Navigator>
    );
}

//bottom tab
const renderIconTabBar = (props, route) => {
    let icon;
    // const getHeight = route === props.state.routes[props.state.index] ? 64 : 36;
    const linkIcon = '../Image/TabIcon/';
    if (route.name === 'Home') {
        icon = <Image
            resizeMode="contain"
            style={{ alignSelf: 'center', height: 32 }}
            source={route === props.state.routes[props.state.index] ? require(linkIcon + 'home_click.png') : require(linkIcon + 'home.png')}
        />;
    } else if (route.name === 'Wallet') {
        icon = <Image
            resizeMode='contain'
            style={{ alignSelf: 'center', height: 32 }}
            source={route === props.state.routes[props.state.index] ? require(linkIcon + 'wallet_click.png') : require(linkIcon + 'wallet.png')}
        />;
    } else if (route.name === 'Sale') {
        icon = <Image
            resizeMode="contain"
            style={{ alignSelf: 'center', height: 32 }}
            source={route === props.state.routes[props.state.index] ? require(linkIcon + 'sale_click.png') : require(linkIcon + 'sale.png')}
        />;
    } else if (route.name === 'Noti') {
        icon = <Image
            resizeMode="contain"
            style={{ alignSelf: 'center', height: 32 }}
            source={route === props.state.routes[props.state.index] ? require(linkIcon + 'noti_click.png') : require(linkIcon + 'noti.png')}
        />;
    } else if (route.name === 'Info') {
        icon = <Image
            resizeMode='contain'
            style={{ alignSelf: 'center', height: 32 }}
            source={route === props.state.routes[props.state.index] ? require(linkIcon + 'info_click.png') : require(linkIcon + 'info.png')}
        />;
    }
    return icon;
};
const renderLabelTabBar = (props, route) => {
    let label;
    const getColor = route === props.state.routes[props.state.index] ? '#76FF60' : '#fff'
    const styleLabel = { color: getColor, textAlign: 'center', fontSize: 11 };
    if (route.name === 'Home') {
        label = <Text style={styleLabel}>Trang chủ</Text>;
    } else if (route.name === 'Wallet') {
        label = <Text style={styleLabel}>Ví cadosa</Text>;
    } else if (route.name === 'Sale') {
        label = <Text style={styleLabel}>Sale</Text>;
    } else if (route.name === 'Noti') {
        label = <Text style={styleLabel}>Thông báo</Text>;
    } else if (route.name === 'Info') {
        label = <Text style={styleLabel}>Tôi</Text>;
    }
    return label;
};
const RenderTabBarNavigation = (prop) => {
    const styleActive = {
        justifyContent: 'space-evenly',
    };
    return (
        <View style={{
            marginBottom: bottomHeight,
            height: height,
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            backgroundColor: '#52BE59'
        }}>
            {
                prop.state.routes.map((route, index) => {
                    return (
                        <TouchableWithoutFeedback
                            onPress={() => {
                                prop.navigation.navigate(route.name);
                            }}
                            key={index}
                        >
                            {
                                prop.state.index === index ?
                                    <View
                                        style={[prop.state.index === index && styleActive]}>
                                        {renderIconTabBar(prop, route)}
                                        {renderLabelTabBar(prop, route)}
                                    </View>
                                    :
                                    <View
                                        style={[prop.state.index === index && styleActive,]}>
                                        {renderIconTabBar(prop, route)}
                                        {renderLabelTabBar(prop, route)}
                                    </View>
                            }
                        </TouchableWithoutFeedback>
                    );
                })
            }
        </View>
    );
};
const Tab = createBottomTabNavigator();
const TabScreen = () => {
    return (
        <Tab.Navigator tabBar={prop => <RenderTabBarNavigation {...prop} />}>
            <Tab.Screen name="Home" component={HomeStackScreen} />
            <Tab.Screen name="Wallet" component={WalletStackScreen} />
            <Tab.Screen name="Sale" component={SaleStackScreen} />
            <Tab.Screen name="Noti" component={NotificationsStackScreen} />
            <Tab.Screen name="Info" component={InfoStackScreen} />
        </Tab.Navigator>
    );
};
export default TabScreen;