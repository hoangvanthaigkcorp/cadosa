const API = {
    baseUrl: 'http://188.166.251.69:6868',
    login:'/v1/auth/login',
    product:'/v1/products/getHotProducts',
    flashSale:'/v1/flashSales/getProductsOnGoingFlashSales',
    post:'/v1/posts',
    cart:'/v1/cart',
    category:'/v1/categories',
};
export default API;
