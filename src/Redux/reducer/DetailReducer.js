import Types from '../types';
import { combineReducers } from 'redux';

const initialState = {
    detail: ''
};

const detailData = (state = initialState, action) => {
    const { type, param } = action;
    switch (type) {
        // Change og code
        case Types.SET_OG_DATA: {
            return {
                // State
                ...state,
                // Redux Store
                detail: param,
            }
        }

        // Default
        default: {
            return state;
        }
    }
};

export default detailData;
