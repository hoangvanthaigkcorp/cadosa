import * as React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Dimensions, Platform, NativeModules } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 40 : 10;
const Header = (props) => {
    return (
        <View style={{ flexDirection: 'row', width: windowWidth, height: 45, backgroundColor: '#52BE59', marginTop: -STATUSBAR_HEIGHT }}>
            
            <TouchableOpacity onPress={() => { }}>
                <Image style={{ marginLeft: 10, marginTop: STATUSBAR_HEIGHT, marginRight: 0.35 * windowWidth }} source={require('../Image/back.png')} />
            </TouchableOpacity>
            <Text style={{ flexDirection: 'row', textAlign: 'center', fontSize: 18, color: '#fff', marginTop: STATUSBAR_HEIGHT, textAlign: "center" }}>{props.title}</Text>
            <TouchableOpacity onPress={() => { props.navigation.navigate('Category') }}>
                <Image style={{ marginLeft: 10, marginTop: STATUSBAR_HEIGHT, marginLeft: 0.29 * windowWidth }} source={require('../Image/menu.png')} />
            </TouchableOpacity>
        </View>
    );
}
export default Header;